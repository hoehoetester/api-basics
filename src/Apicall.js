import React, { Component } from 'react';
// import axios from 'axios';

class Apicall extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            subreddit: 'space'
        };
        this.getReddit = this.getReddit.bind(this);
    }

    componentWillMount() {
        this.getReddit();
    }

    getReddit() {
        // axios.get(`https://www.reddit.com/r/${this.state.subreddit}.json`)
        //     .then(res => {
        //         const posts = res.data.data.children.map(obj => obj.data);
        //         this.setState({ posts });
        //     });
        fetch(`https://www.reddit.com/r/${this.state.subreddit}.json`)
            .then(res => res.json())
            .then(data => {
                const posts = data.data.children.map(obj => obj.data);
                this.setState({ posts });
            });
    }

    render() {
        return (
            <div>
                <h1>{`about "${this.state.subreddit}" from Reddit.com`}</h1>
                <ul>
                    {this.state.posts.map(post =>
                        <li key={post.id}>{post.title}</li>)}
                </ul>
            </div>
        );
    }
}

export default Apicall;